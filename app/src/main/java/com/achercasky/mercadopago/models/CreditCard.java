package com.achercasky.mercadopago.models;

import com.achercasky.mercadopago.utils.JsonKeys;
import com.squareup.moshi.Json;

/**
 * Created by achercasky on 24/08/2017.
 */

public class CreditCard {

    @Json(name = JsonKeys.ID)
    private String id;

    @Json(name = JsonKeys.NAME)
    private String name;

    @Json(name = JsonKeys.PAYMENT_TYPE_ID)
    private String paymentTypeId;

    @Json(name = JsonKeys.THUMBNAIL)
    private String thumbnail;

    @Json(name = JsonKeys.MAX_ALLOWED_AMOUNT)
    private String maxAllowedAmount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    public void setMaxAllowedAmount(String maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }
}
