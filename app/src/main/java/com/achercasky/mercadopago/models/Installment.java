package com.achercasky.mercadopago.models;

import com.achercasky.mercadopago.utils.JsonKeys;
import com.squareup.moshi.Json;

import java.util.List;

/**
 * Created by achercasky on 24/08/2017.
 */

public class Installment {

    @Json(name = JsonKeys.PAYER_COSTS)
    private List<PayerCost> payerCosts;

    public List<PayerCost> getPayerCosts() {
        return payerCosts;
    }

    public void setPayerCosts(List<PayerCost> payerCosts) {
        this.payerCosts = payerCosts;
    }
}
