package com.achercasky.mercadopago.models;

import com.achercasky.mercadopago.utils.JsonKeys;
import com.squareup.moshi.Json;

/**
 * Created by achercasky on 24/08/2017.
 */

public class PayerCost {

    @Json(name = JsonKeys.RECOMMENDED_MESSAGE)
    private String recommendedMessage;

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }
}
