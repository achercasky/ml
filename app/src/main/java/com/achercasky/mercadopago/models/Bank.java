package com.achercasky.mercadopago.models;

import com.achercasky.mercadopago.utils.JsonKeys;
import com.squareup.moshi.Json;

/**
 * Created by achercasky on 24/08/2017.
 */

public class Bank {

    @Json(name = JsonKeys.ID)
    private String id;

    @Json(name = JsonKeys.NAME)
    private String name;

    @Json(name = JsonKeys.THUMBNAIL)
    private String thumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
