package com.achercasky.mercadopago.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.achercasky.mercadopago.R;
import com.achercasky.mercadopago.models.Bank;

import com.achercasky.mercadopago.mvp.presenters.BankPresenter;
import com.achercasky.mercadopago.mvp.views.BankView;
import com.achercasky.mercadopago.repositories.UserRepository;
import com.achercasky.mercadopago.ui.activities.InstallmentActivity;
import com.achercasky.mercadopago.ui.adapters.BankAdapter;

import com.achercasky.mercadopago.ui.views.BankItemView;
import com.achercasky.mercadopago.utils.SnackbarUtils;

import java.util.List;

import butterknife.BindView;

/**
 * Created by achercasky on 24/08/2017.
 */

public class BankFragment extends BaseFragment implements BankView, BankItemView.Action{

    @BindView(R.id.bank_progress) ProgressBar progressBar;

    @BindView(R.id.bank_recycler) RecyclerView recyclerView;

    private BankAdapter adapter;

    private BankPresenter presenter;

    public static BankFragment newInstance() {

        Bundle args = new Bundle();

        BankFragment fragment = new BankFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getMainLayoutResId() {
        return R.layout.fragment_bank;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new BankPresenter();
        presenter.attachMvpView(this);

        adapter = new BankAdapter();
        adapter.setAction(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        presenter.getBanks(UserRepository.getInstance().getCreditCardId());
    }

    @Override
    public void onBanksReceived(List<Bank> bankList) {

        setToolBar(getString(R.string.bank_title), true);

        progressBar.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);

        adapter.setBankList(bankList);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onEmptyBanks() {
        progressBar.setVisibility(View.GONE);

        UserRepository.getInstance().setBankId("");
        UserRepository.getInstance().setBankName("");

        changeFragment(InstallmentFragment.newInstance(), false);
    }

    @Override
    public Context getMvpContext() {
        return getActivity();
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onNoInternetConnection() {
        progressBar.setVisibility(View.GONE);

        SnackbarUtils.show(recyclerView, getString(R.string.no_connection), getString(R.string.retry),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressBar.setVisibility(View.VISIBLE);
                        presenter.getBanks(UserRepository.getInstance().getCreditCardId());
                    }
                });
    }

    @Override
    public void onErrorCode(String s) {

    }

    @Override
    public void onBankSelected(Bank bank) {
        UserRepository.getInstance().setBankId(bank.getId());
        UserRepository.getInstance().setBankName(bank.getName());

        InstallmentActivity.start(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();

        presenter.dettachMvpView();
    }
}
