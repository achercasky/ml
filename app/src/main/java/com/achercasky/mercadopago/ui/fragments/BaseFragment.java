package com.achercasky.mercadopago.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.achercasky.mercadopago.ui.activities.BaseActivity;

import butterknife.ButterKnife;

/**
 * Created by achercasky on 24/08/2017.
 */

public abstract class BaseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getMainLayoutResId(), container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    protected void changeFragment(Fragment fragment, boolean addToBackStack) {
        ((BaseActivity)getActivity()).changeFragment(fragment, addToBackStack);
    }

    protected void setToolBar(String title, boolean shouldGoBack) {
        ((BaseActivity) getActivity()).getSupportActionBar().setTitle(title);
        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(shouldGoBack);
    }

    protected abstract int getMainLayoutResId();
}
