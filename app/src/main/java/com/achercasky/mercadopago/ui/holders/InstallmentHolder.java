package com.achercasky.mercadopago.ui.holders;

import android.view.View;

import com.achercasky.mercadopago.models.PayerCost;
import com.achercasky.mercadopago.ui.views.InstallmentItemView;

/**
 * Created by achercasky on 24/08/2017.
 */

public class InstallmentHolder extends BaseViewHolder {

    private InstallmentItemView view;

    public InstallmentHolder(View itemView) {
        super(itemView);

        this.view = (InstallmentItemView) itemView;
    }

    @Override
    public void setViewData(Object object) {
        view.loadContent((PayerCost) object);
    }
}
