package com.achercasky.mercadopago.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.achercasky.mercadopago.R;
import com.achercasky.mercadopago.ui.fragments.PaymentMethodFragment;

/**
 * Created by achercasky on 25/08/2017.
 */

public class PaymentMethodActivity extends BaseActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, PaymentMethodActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolBar(getString(R.string.payment_method_title), true);
    }

    @Override
    protected void setInitialFragment() {
      changeFragment(PaymentMethodFragment.newInstance(), false);
    }

    @Override
    public boolean isSplashScreen() {
        return false;
    }

    @Override
    public void bindView() {

    }

    @Override
    public void injectClass() {

    }
}
