package com.achercasky.mercadopago.ui.holders;

import android.view.View;

import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.ui.views.BankItemView;

/**
 * Created by achercasky on 24/08/2017.
 */

public class BankHolder extends BaseViewHolder {

    private BankItemView view;

    public BankHolder(View itemView) {
        super(itemView);

        this.view = (BankItemView) itemView;
    }

    @Override
    public void setViewData(Object object) {
        view.loadContent((Bank) object);
    }
}
