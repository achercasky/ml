package com.achercasky.mercadopago.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.achercasky.mercadopago.R;
import com.achercasky.mercadopago.models.PayerCost;
import com.achercasky.mercadopago.mvp.presenters.InstallmentPresenter;
import com.achercasky.mercadopago.mvp.views.InstallmentView;
import com.achercasky.mercadopago.repositories.UserRepository;
import com.achercasky.mercadopago.ui.activities.HomeActivity;
import com.achercasky.mercadopago.ui.adapters.InstallmentAdapter;
import com.achercasky.mercadopago.ui.views.InstallmentItemView;
import com.achercasky.mercadopago.utils.SnackbarUtils;

import java.util.List;

import butterknife.BindView;

/**
 * Created by achercasky on 24/08/2017.
 */

public class InstallmentFragment extends BaseFragment implements InstallmentView, InstallmentItemView.Action{

    @BindView(R.id.installment_progress) ProgressBar progressBar;

    @BindView(R.id.installment_recycler) RecyclerView recyclerView;

    private InstallmentAdapter adapter;

    private InstallmentPresenter presenter;

    public static InstallmentFragment newInstance() {

        Bundle args = new Bundle();

        InstallmentFragment fragment = new InstallmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getMainLayoutResId() {
        return R.layout.fragment_installment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setToolBar(getString(R.string.installment_title), true);

        presenter = new InstallmentPresenter();
        presenter.attachMvpView(this);

        adapter = new InstallmentAdapter();
        adapter.setAction(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        presenter.getInstallments(UserRepository.getInstance().getAmount(), UserRepository.getInstance().getCreditCardId(),
                UserRepository.getInstance().getBankId());
    }

    @Override
    public void onPayerCostReceived(List<PayerCost> payerCosts) {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        adapter.setPayerCostList(payerCosts);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onEmptyInstallment() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public Context getMvpContext() {
        return getActivity();
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onNoInternetConnection() {
        progressBar.setVisibility(View.GONE);

        SnackbarUtils.show(recyclerView, getString(R.string.no_connection), getString(R.string.retry),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressBar.setVisibility(View.VISIBLE);
                        presenter.getInstallments(UserRepository.getInstance().getAmount(),
                                UserRepository.getInstance().getCreditCardId(), UserRepository.getInstance().getBankId());
                    }
                });
    }

    @Override
    public void onErrorCode(String s) {

    }

    @Override
    public void onRecommendedInstallmentSelected(PayerCost payerCost) {
        UserRepository.getInstance().setRecommendedMessage(payerCost.getRecommendedMessage());

        HomeActivity.start(getContext(), true);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        presenter.dettachMvpView();
    }
}
