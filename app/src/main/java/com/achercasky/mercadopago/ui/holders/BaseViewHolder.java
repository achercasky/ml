package com.achercasky.mercadopago.ui.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by achercasky on 24/08/2017.
 */

public abstract class BaseViewHolder extends RecyclerView.ViewHolder{

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void setViewData(Object object);
}
