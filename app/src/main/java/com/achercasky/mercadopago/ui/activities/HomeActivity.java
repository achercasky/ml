package com.achercasky.mercadopago.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.achercasky.mercadopago.R;
import com.achercasky.mercadopago.ui.fragments.HomeFragment;

public class HomeActivity extends BaseActivity {

    public static void start(Context context, boolean isFinish) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.putExtra(HomeFragment.TAG, isFinish);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolBar(getString(R.string.app_name), false);
    }

    @Override
    protected void setInitialFragment() {

        if(getIntent().getBooleanExtra(HomeFragment.TAG, false)) {
            changeFragment(HomeFragment.newInstance(true), false);
        } else {
            changeFragment(HomeFragment.newInstance(), false);
        }
    }

    @Override
    public boolean isSplashScreen() {
        return false;
    }

    @Override
    public void bindView() {
        //Nothing to do here...
    }

    @Override
    public void injectClass() {
        //Nothing to do here...
    }

}
