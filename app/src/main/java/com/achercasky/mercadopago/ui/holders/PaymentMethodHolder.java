package com.achercasky.mercadopago.ui.holders;

import android.view.View;

import com.achercasky.mercadopago.models.CreditCard;
import com.achercasky.mercadopago.ui.views.PaymentMethodItemView;

/**
 * Created by achercasky on 24/08/2017.
 */

public class PaymentMethodHolder extends BaseViewHolder {

    private PaymentMethodItemView view;

    public PaymentMethodHolder(View itemView) {
        super(itemView);

        this.view = (PaymentMethodItemView) itemView;
    }

    @Override
    public void setViewData(Object object) {
        view.loadContent((CreditCard) object);
    }
}
