package com.achercasky.mercadopago.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.achercasky.mercadopago.ui.fragments.BankFragment;

/**
 * Created by achercasky on 25/08/2017.
 */

public class BankActivity extends BaseActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, BankActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolBar("", true);
    }

    @Override
    protected void setInitialFragment() {
        changeFragment(BankFragment.newInstance(), false);
    }

    @Override
    public boolean isSplashScreen() {
        return false;
    }

    @Override
    public void bindView() {

    }

    @Override
    public void injectClass() {

    }
}
