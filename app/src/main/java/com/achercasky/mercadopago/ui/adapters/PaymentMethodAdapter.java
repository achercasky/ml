package com.achercasky.mercadopago.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.achercasky.mercadopago.models.CreditCard;
import com.achercasky.mercadopago.ui.holders.BaseViewHolder;
import com.achercasky.mercadopago.ui.holders.PaymentMethodHolder;
import com.achercasky.mercadopago.ui.views.PaymentMethodItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by achercasky on 24/08/2017.
 */

public class PaymentMethodAdapter extends RecyclerView.Adapter<BaseViewHolder>{

    private List<CreditCard> creditCardList;

    private PaymentMethodItemView.Action action;

    public PaymentMethodAdapter() {
        creditCardList = new ArrayList<>();
    }

    public void setCreditCardList(List<CreditCard> creditCardList) {
        this.creditCardList = creditCardList;
    }

    public void setAction(PaymentMethodItemView.Action action) {
        this.action = action;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PaymentMethodHolder(new PaymentMethodItemView(parent.getContext(), action));
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        CreditCard creditCard = creditCardList.get(position);

        holder.setViewData(creditCard);
    }

    @Override
    public int getItemCount() {
        return creditCardList.size();
    }
}
