package com.achercasky.mercadopago.ui.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.achercasky.mercadopago.R;
import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.models.CreditCard;
import com.achercasky.mercadopago.utils.glide.GlideApp;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by achercasky on 24/08/2017.
 */

public class BankItemView extends LinearLayout{

    @BindView(R.id.item_bank_title) TextView title;

    @BindView(R.id.item_bank_image) ImageView image;

    @BindView(R.id.item_payment_method_image_container) CardView container;

    @BindView(R.id.item_bank_progress) ProgressBar progressBar;

    private Action action;

    public BankItemView(Context context, Action action) {
        super(context);

        this.action = action;

        inflateViews();
    }

    private void inflateViews() {
        inflate(getContext(), R.layout.item_bank, this);

        ButterKnife.bind(this);
    }

    public void loadContent(final Bank bank) {
        title.setText(bank.getName());

        GlideApp.with(getContext())
                .load(bank.getThumbnail())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target,
                                                boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource,
                                                   boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(image);

        container.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                action.onBankSelected(bank);
            }
        });
    }

    public interface Action {
        void onBankSelected(Bank bank);
    }
}
