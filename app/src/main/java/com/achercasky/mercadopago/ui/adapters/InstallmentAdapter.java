package com.achercasky.mercadopago.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.models.PayerCost;
import com.achercasky.mercadopago.ui.holders.BankHolder;
import com.achercasky.mercadopago.ui.holders.BaseViewHolder;
import com.achercasky.mercadopago.ui.holders.InstallmentHolder;
import com.achercasky.mercadopago.ui.views.BankItemView;
import com.achercasky.mercadopago.ui.views.InstallmentItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by achercasky on 24/08/2017.
 */

public class InstallmentAdapter extends RecyclerView.Adapter<BaseViewHolder>{

    private List<PayerCost> payerCostList;

    private InstallmentItemView.Action action;

    public InstallmentAdapter() {
        payerCostList = new ArrayList<>();
    }

    public void setPayerCostList(List<PayerCost> payerCostList) {
        this.payerCostList = payerCostList;
    }

    public void setAction(InstallmentItemView.Action action) {
        this.action = action;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InstallmentHolder(new InstallmentItemView(parent.getContext(), action));
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        PayerCost creditCard = payerCostList.get(position);

        holder.setViewData(creditCard);
    }

    @Override
    public int getItemCount() {
        return payerCostList.size();
    }
}
