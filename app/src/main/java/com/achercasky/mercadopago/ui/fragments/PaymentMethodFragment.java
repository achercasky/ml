package com.achercasky.mercadopago.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.achercasky.mercadopago.R;
import com.achercasky.mercadopago.models.CreditCard;
import com.achercasky.mercadopago.mvp.presenters.PaymentMethodPresenter;
import com.achercasky.mercadopago.mvp.views.PaymentMethodView;
import com.achercasky.mercadopago.repositories.UserRepository;
import com.achercasky.mercadopago.ui.activities.BankActivity;
import com.achercasky.mercadopago.ui.adapters.PaymentMethodAdapter;
import com.achercasky.mercadopago.ui.views.PaymentMethodItemView;
import com.achercasky.mercadopago.utils.SnackbarUtils;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;

/**
 * Created by achercasky on 24/08/2017.
 */

public class PaymentMethodFragment extends BaseFragment implements PaymentMethodView, PaymentMethodItemView.Action{

    @BindView(R.id.payment_method_progress) ProgressBar progressBar;

    @BindView(R.id.payment_method_recycler) RecyclerView recyclerView;

    private PaymentMethodAdapter adapter;

    private PaymentMethodPresenter presenter;

    public static PaymentMethodFragment newInstance() {

        Bundle args = new Bundle();

        PaymentMethodFragment fragment = new PaymentMethodFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getMainLayoutResId() {
        return R.layout.fragment_payment_method;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new PaymentMethodPresenter();
        presenter.attachMvpView(this);

        adapter = new PaymentMethodAdapter();
        adapter.setAction(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        presenter.getPaymentMethods();
    }

    @Override
    public void onCreditCardListReceived(List<CreditCard> creditCardList) {

        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        adapter.setCreditCardList(creditCardList);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onExceededAmount(double maxAmount) {
        DecimalFormat format = new DecimalFormat("0.#");

        SnackbarUtils.show(getView(), getString(R.string.payment_exceed_amount) + " " + format.format(maxAmount));
    }

    @Override
    public void onGoToBankScreen(CreditCard creditCard) {
        UserRepository.getInstance().setCreditCardId(creditCard.getId());
        UserRepository.getInstance().setCreditCardName(creditCard.getName());

        BankActivity.start(getActivity());
    }

    @Override
    public Context getMvpContext() {
        return getActivity();
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onNoInternetConnection() {

        progressBar.setVisibility(View.GONE);

        SnackbarUtils.show(recyclerView, getString(R.string.no_connection), getString(R.string.retry),
                new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    progressBar.setVisibility(View.VISIBLE);
                    presenter.getPaymentMethods();
                }
            });
    }

    @Override
    public void onErrorCode(String s) {

    }

    @Override
    public void onCreditCardClicked(CreditCard creditCard) {
        presenter.validateAmount(creditCard, Double.parseDouble(UserRepository.getInstance().getAmount()));
    }

    @Override
    public void onResume() {
        super.onResume();

        UserRepository.getInstance().setCreditCardId("");
        UserRepository.getInstance().setCreditCardName("");
    }

    @Override
    public void onDetach() {
        super.onDetach();

        presenter.dettachMvpView();
    }
}
