package com.achercasky.mercadopago.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.ui.holders.BankHolder;
import com.achercasky.mercadopago.ui.holders.BaseViewHolder;
import com.achercasky.mercadopago.ui.views.BankItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by achercasky on 24/08/2017.
 */

public class BankAdapter extends RecyclerView.Adapter<BaseViewHolder>{

    private List<Bank> bankList;

    private BankItemView.Action action;

    public BankAdapter() {
        bankList = new ArrayList<>();
    }

    public void setBankList(List<Bank> bankList) {
        this.bankList = bankList;
    }

    public void setAction(BankItemView.Action action) {
        this.action = action;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BankHolder(new BankItemView(parent.getContext(), action));
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        Bank creditCard = bankList.get(position);

        holder.setViewData(creditCard);
    }

    @Override
    public int getItemCount() {
        return bankList.size();
    }
}
