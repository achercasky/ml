package com.achercasky.mercadopago.ui.views;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.achercasky.mercadopago.R;
import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.models.PayerCost;
import com.achercasky.mercadopago.utils.glide.GlideApp;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by achercasky on 24/08/2017.
 */

public class InstallmentItemView extends LinearLayout{

    @BindView(R.id.item_installment_title) TextView title;

    @BindView(R.id.item_installment_container) LinearLayout container;

    private Action action;

    public InstallmentItemView(Context context, Action action) {
        super(context);

        this.action = action;

        inflateViews();
    }

    private void inflateViews() {
        inflate(getContext(), R.layout.item_installment, this);

        ButterKnife.bind(this);
    }

    public void loadContent(final PayerCost payerCost) {
        title.setText(payerCost.getRecommendedMessage());

        container.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                action.onRecommendedInstallmentSelected(payerCost);
            }
        });
    }

    public interface Action {
        void onRecommendedInstallmentSelected(PayerCost payerCost);
    }
}
