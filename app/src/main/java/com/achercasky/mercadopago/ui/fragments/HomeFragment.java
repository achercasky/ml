package com.achercasky.mercadopago.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.achercasky.mercadopago.R;
import com.achercasky.mercadopago.mvp.presenters.HomePresenter;
import com.achercasky.mercadopago.mvp.views.HomeView;
import com.achercasky.mercadopago.repositories.UserRepository;
import com.achercasky.mercadopago.ui.activities.PaymentMethodActivity;
import com.achercasky.mercadopago.utils.Utils;

import butterknife.BindView;

/**
 * Created by achercasky on 24/08/2017.
 */

public class HomeFragment extends BaseFragment implements HomeView{

    @BindView(R.id.home_input)
    EditText input;

    @BindView(R.id.home_next) Button next;

    public static final String TAG = HomeFragment.class.getSimpleName();

    private HomePresenter presenter;

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static HomeFragment newInstance(boolean isFromLastScreen) {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        args.putBoolean(TAG, isFromLastScreen);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getMainLayoutResId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new HomePresenter();
        presenter.attachMvpView(this);

        if(getArguments().getBoolean(TAG)) {
            Utils.showDialog(getActivity(), getString(R.string.home_dialog_title), UserRepository.getInstance().showInfo());
            UserRepository.getInstance().reset();
        }

        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_DONE) {
                   presenter.validate(input.getText().toString());
                    return true;
                }
                return false;
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.validate(input.getText().toString());;
            }
        });
    }

    private void goToBankScreen() {
        UserRepository.getInstance().setAmount(String.valueOf(input.getText()));
        PaymentMethodActivity.start(getActivity());
    }

    @Override
    public void onEmptyAmount() {
        Toast.makeText(getContext(), getString(R.string.home_empty_amount), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInvalidAmount() {
        Toast.makeText(getContext(), getString(R.string.home_no_zero), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onValidAmount() {
        goToBankScreen();
    }

    @Override
    public Context getMvpContext() {
        return getActivity();
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onNoInternetConnection() {

    }

    @Override
    public void onErrorCode(String s) {

    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.dettachMvpView();
    }
}
