package com.achercasky.mercadopago.ui.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.achercasky.mercadopago.R;
import com.achercasky.mercadopago.models.CreditCard;
import com.achercasky.mercadopago.utils.glide.GlideApp;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by achercasky on 24/08/2017.
 */

public class PaymentMethodItemView extends RelativeLayout{

    @BindView(R.id.item_payment_method_title) TextView title;

    @BindView(R.id.item_payment_method_image) ImageView image;

    @BindView(R.id.item_payment_method_progress) ProgressBar progressBar;

    @BindView(R.id.item_payment_method_image_container) RelativeLayout container;

    private Action action;

    public PaymentMethodItemView(Context context, Action action) {
        super(context);

        this.action = action;

        inflateViews();
    }

    private void inflateViews() {
        inflate(getContext(), R.layout.item_payment_method, this);

        ButterKnife.bind(this);
    }

    public void loadContent(final CreditCard creditCard) {
        title.setText(creditCard.getName());

        GlideApp.with(getContext())
                .load(creditCard.getThumbnail())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target,
                                                boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource,
                                                   boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(image);

        container.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                action.onCreditCardClicked(creditCard);
            }
        });
    }

    public interface Action {
        void onCreditCardClicked(CreditCard creditCard);
    }
}
