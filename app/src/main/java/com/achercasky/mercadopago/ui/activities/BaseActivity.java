package com.achercasky.mercadopago.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.achercasky.mercadopago.R;
import com.chachapps.initialclasses.activity.InitialActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by achercasky on 24/08/2017.
 */

public abstract class BaseActivity extends InitialActivity{

    @BindView(R.id.frame)
    FrameLayout frameLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        ButterKnife.bind(this);

        setInitialFragment();
    }

    @Override
    public int getMyFragment() {
        return frameLayout.getId();
    }

    /**
     * This method is used for the BaseFragment class.
     * @param fragment
     */
    public void changeFragment(Fragment fragment, boolean addToBackStack) {
        changeFragment(fragment, addToBackStack, FragmentTransaction.TRANSIT_NONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void setToolBar(String title, boolean shouldGoBack) {
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(shouldGoBack);
    }

    protected abstract void setInitialFragment();
}
