package com.achercasky.mercadopago.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by achercasky on 25/08/2017.
 */

public class SnackbarUtils {

    public static void show(View view, String message) {
        Snackbar mySnackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        mySnackbar.show();
    }

    public static void show(View view, String message, String actionMessage, View.OnClickListener onClickListener) {
        Snackbar mySnackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
        mySnackbar.setAction(actionMessage, onClickListener);
        mySnackbar.show();
    }
}
