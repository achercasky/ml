package com.achercasky.mercadopago.utils.glide;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by achercasky on 24/08/2017.
 */

@GlideModule
public class MyAppGlideModule extends AppGlideModule {
}
