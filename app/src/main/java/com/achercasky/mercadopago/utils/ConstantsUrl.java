package com.achercasky.mercadopago.utils;

/**
 * Created by arielchercasky on 28/8/17.
 */

public class ConstantsUrl {

    public static final String PARAMETER_PAYMENT_METHODS = "payment_methods";
    public static final String PARAMETER_CARD_ISSUES = "card_issuers";

    public static final String PAYMENT_METHOD_ID = "payment_method_id";
    public static final String INSTALLMENTS = "installments";
    public static final String AMOUNT = "amount";
    public static final String ISSUER_ID = "issuer.id";


    public static final String GET_BANKS = PARAMETER_PAYMENT_METHODS + "/" + PARAMETER_CARD_ISSUES;
    public static final String GET_INSTALLMENT = PARAMETER_PAYMENT_METHODS + "/" + INSTALLMENTS;


}
