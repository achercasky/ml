package com.achercasky.mercadopago.utils;

/**
 * Created by achercasky on 24/08/2017.
 */

public class Constants {

    public static final String PUBLIC_KEY = "public_key";

    public static final String CREDIT_CARD = "credit_card";

}
