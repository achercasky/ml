package com.achercasky.mercadopago.utils;

/**
 * Created by arielchercasky on 28/8/17.
 */

public class JsonKeys {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String PAYMENT_TYPE_ID = "payment_type_id";
    public static final String THUMBNAIL = "thumbnail";
    public static final String MAX_ALLOWED_AMOUNT = "max_allowed_amount";
    public static final String PAYER_COSTS = "payer_costs";
    public static final String RECOMMENDED_MESSAGE = "recommended_message";
}
