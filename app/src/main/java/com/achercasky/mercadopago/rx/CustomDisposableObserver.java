package com.achercasky.mercadopago.rx;

import android.util.Log;

import java.net.ConnectException;
import java.net.UnknownHostException;

import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

/**
 * Created by achercasky on 24/08/2017.
 */

public abstract class CustomDisposableObserver<T> extends DisposableObserver<T> {

    private static final String TAG = CustomDisposableObserver.class.getSimpleName();

    private String response;
    private String url;
    private String message;

    private int errorCode;

    @Override
    public void onError(Throwable e) {

        if(e instanceof HttpException) {
            Log.e(TAG,"-----------------------------------------------------------------------------------------------");
            Log.e(TAG, "Error: " + ((HttpException) e).response().raw().toString());
            Log.e(TAG,"-----------------------------------------------------------------------------------------------");

            errorCode = ((HttpException) e).code();
            message = e.getMessage();
            url = ((HttpException) e).response().raw().request().url().toString();

            try {
                response = ((HttpException) e).response().errorBody().string();

                onErrorCode(response);

            }catch (Exception e1) {
                onObserverError(e);
            }

        }else if(e instanceof ConnectException || e instanceof UnknownHostException){
            onNoInternetConnection();
        } else {
            onObserverError(e);
        }

    }

    public abstract void onNoInternetConnection();
    public abstract void onObserverError(Throwable e);
    public abstract void onErrorCode(String message);
}
