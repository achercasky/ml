package com.achercasky.mercadopago.mvp.views;

import com.achercasky.mercadopago.models.Installment;
import com.achercasky.mercadopago.models.PayerCost;
import com.chachapps.initialclasses.mvp.view.BaseMvpView;

import java.util.List;

/**
 * Created by achercasky on 24/08/2017.
 */

public interface InstallmentView extends BaseMvpView{

    void onPayerCostReceived(List<PayerCost> payerCosts);
    void onEmptyInstallment();
}
