package com.achercasky.mercadopago.mvp.presenters;

import com.achercasky.mercadopago.api.MercadoPagoService;
import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.mvp.views.BankView;
import com.achercasky.mercadopago.rx.CustomDisposableObserver;
import com.chachapps.initialclasses.mvp.presenter.BasePresenter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by achercasky on 24/08/2017.
 */

public class BankPresenter extends BasePresenter<BankView> {

    public void getBanks(String id){
        Disposable disposable = MercadoPagoService.getInstance().getBanks(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new CustomDisposableObserver<List<Bank>>() {
                    @Override
                    public void onNext(List<Bank> bankList) {

                        if(bankList.size() > 0) {
                            getMvpView().onBanksReceived(bankList);
                        } else {
                            getMvpView().onEmptyBanks();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNoInternetConnection() {
                        getMvpView().onNoInternetConnection();
                    }

                    @Override
                    public void onObserverError(Throwable e) {

                    }

                    @Override
                    public void onErrorCode(String message) {

                    }
                });

        compositeSubscription.add(disposable);
    }
}
