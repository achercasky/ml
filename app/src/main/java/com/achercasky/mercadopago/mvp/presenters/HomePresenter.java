package com.achercasky.mercadopago.mvp.presenters;

import com.achercasky.mercadopago.mvp.views.HomeView;
import com.chachapps.initialclasses.mvp.presenter.BasePresenter;

/**
 * Created by achercasky on 26/08/2017.
 */

public class HomePresenter extends BasePresenter<HomeView> {

    public void validate(String amount) {

        if(amount.equals("")) {
            getMvpView().onEmptyAmount();
            return;
        }

        if(amount.matches("[0]+")) {
            getMvpView().onInvalidAmount();
            return;
        }

        getMvpView().onValidAmount();
    }
}
