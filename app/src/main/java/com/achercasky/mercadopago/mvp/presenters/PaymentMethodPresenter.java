package com.achercasky.mercadopago.mvp.presenters;

import com.achercasky.mercadopago.api.MercadoPagoService;
import com.achercasky.mercadopago.models.CreditCard;
import com.achercasky.mercadopago.mvp.views.PaymentMethodView;
import com.achercasky.mercadopago.rx.CustomDisposableObserver;
import com.achercasky.mercadopago.utils.Constants;
import com.chachapps.initialclasses.mvp.presenter.BasePresenter;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by achercasky on 24/08/2017.
 */

public class PaymentMethodPresenter extends BasePresenter<PaymentMethodView> {

    public void getPaymentMethods(){
        Disposable disposable = MercadoPagoService.getInstance().getPaymentMethods()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new CustomDisposableObserver<List<CreditCard>>() {
                    @Override
                    public void onNext(List<CreditCard> creditCardList) {

                        filterByCreditCards(creditCardList);
                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNoInternetConnection() {
                        getMvpView().onNoInternetConnection();
                    }

                    @Override
                    public void onObserverError(Throwable e) {

                    }

                    @Override
                    public void onErrorCode(String message) {

                    }
                });

        compositeSubscription.add(disposable);
    }

    private void filterByCreditCards(final List<CreditCard> creditCardList) {
        Observable.fromIterable(creditCardList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(new Predicate<CreditCard>() {
                    @Override
                    public boolean test(CreditCard creditCard) throws Exception {
                        return creditCard.getPaymentTypeId().equals(Constants.CREDIT_CARD);

                    }
                }).toList()
                .subscribe(new DisposableSingleObserver<List<CreditCard>>() {
                    @Override
                    public void onSuccess(List<CreditCard> value) {
                        getMvpView().onCreditCardListReceived(value);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    public void validateAmount(CreditCard creditCard, double amount) {

        double maxAmount = Double.parseDouble(creditCard.getMaxAllowedAmount());

        if(amount > maxAmount) {
            getMvpView().onExceededAmount(maxAmount);
        } else {
            getMvpView().onGoToBankScreen(creditCard);
        }
    }
}
