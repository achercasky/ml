package com.achercasky.mercadopago.mvp.views;

import com.chachapps.initialclasses.mvp.view.BaseMvpView;

/**
 * Created by achercasky on 26/08/2017.
 */

public interface HomeView extends BaseMvpView {

    void onEmptyAmount();
    void onInvalidAmount();
    void onValidAmount();
}
