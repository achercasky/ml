package com.achercasky.mercadopago.mvp.presenters;

import com.achercasky.mercadopago.api.MercadoPagoService;
import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.models.CreditCard;
import com.achercasky.mercadopago.models.Installment;
import com.achercasky.mercadopago.models.PayerCost;
import com.achercasky.mercadopago.mvp.views.InstallmentView;
import com.achercasky.mercadopago.rx.CustomDisposableObserver;
import com.chachapps.initialclasses.mvp.presenter.BasePresenter;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by achercasky on 24/08/2017.
 */

public class InstallmentPresenter extends BasePresenter<InstallmentView> {

    public void getInstallments(String amount, String paymentMethodId, String issuerId){
        Disposable disposable = MercadoPagoService.getInstance().getInstallments(amount, paymentMethodId, issuerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new CustomDisposableObserver<List<Installment>>() {
                    @Override
                    public void onNext(List<Installment> installments) {

                        if(installments.size() > 0) {
                            getPayerCosts(installments);
                        } else {
                            getMvpView().onEmptyInstallment();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNoInternetConnection() {
                        getMvpView().onNoInternetConnection();
                    }

                    @Override
                    public void onObserverError(Throwable e) {

                    }

                    @Override
                    public void onErrorCode(String message) {

                    }
                });

        compositeSubscription.add(disposable);
    }

    private void getPayerCosts(final List<Installment> creditCardList) {
        Observable.fromIterable(creditCardList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<Installment, List<PayerCost>>() {
                    @Override
                    public List<PayerCost> apply(Installment installment) throws Exception {
                        return installment.getPayerCosts();
                    }
                })
                .subscribe(new DisposableObserver<List<PayerCost>>() {
                    @Override
                    public void onNext(List<PayerCost> value) {
                        getMvpView().onPayerCostReceived(value);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
