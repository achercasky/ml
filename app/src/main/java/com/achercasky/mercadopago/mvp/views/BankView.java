package com.achercasky.mercadopago.mvp.views;

import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.models.CreditCard;
import com.chachapps.initialclasses.mvp.view.BaseMvpView;

import java.util.List;

/**
 * Created by achercasky on 24/08/2017.
 */

public interface BankView extends BaseMvpView{

    void onBanksReceived(List<Bank> bankList);
    void onEmptyBanks();
}
