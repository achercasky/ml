package com.achercasky.mercadopago.api;

import com.achercasky.mercadopago.BuildConfig;
import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.models.CreditCard;
import com.achercasky.mercadopago.models.Installment;
import com.achercasky.mercadopago.utils.Constants;
import com.squareup.moshi.Moshi;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by achercasky on 24/08/2017.
 */

public class MercadoPagoService {

    private static MercadoPagoService instance;

    private MercadoPagoApi api;
    private Retrofit retrofit;

    private static final int TIMEOUT = 30;

    public static MercadoPagoService getInstance() {
        if(instance == null) {
            instance = new MercadoPagoService();
        }
        return instance;
    }

    private MercadoPagoService() {
        if(retrofit == null && api == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.URL)
                    .client(getOkHttpClient())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(MoshiConverterFactory.create(getMoshi()))
                    .build();

            api = retrofit.create(MercadoPagoApi.class);
        }
    }

    private Moshi getMoshi() {
        return new Moshi.Builder().build();
    }

    private OkHttpClient getOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        Interceptor headerInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .setQueryParameter(Constants.PUBLIC_KEY, BuildConfig.PUBLIC_KEY)
                        .build();

                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        };

        return new OkHttpClient.Builder().connectTimeout(TIMEOUT, TimeUnit.SECONDS).readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addNetworkInterceptor(headerInterceptor)
                .build();
    }

    public Observable<List<CreditCard>> getPaymentMethods() {
        return api.getPaymentMethods();
    }

    public Observable<List<Bank>> getBanks(String id) {
        return api.getBanks(id);
    }

    public Observable<List<Installment>> getInstallments(String amount, String paymentMethodId, String issuerId) {
        return api.getInstallments(amount, paymentMethodId, issuerId);
    }
}
