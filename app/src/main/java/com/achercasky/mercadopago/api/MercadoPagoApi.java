package com.achercasky.mercadopago.api;

import com.achercasky.mercadopago.models.Bank;
import com.achercasky.mercadopago.models.CreditCard;
import com.achercasky.mercadopago.models.Installment;
import com.achercasky.mercadopago.utils.ConstantsUrl;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by achercasky on 24/08/2017.
 */

public interface MercadoPagoApi {

    @GET(ConstantsUrl.PARAMETER_PAYMENT_METHODS)
    Observable<List<CreditCard>> getPaymentMethods();

    @GET(ConstantsUrl.GET_BANKS)
    Observable<List<Bank>> getBanks(@Query(ConstantsUrl.PAYMENT_METHOD_ID) String paymentMethodId);

    @GET(ConstantsUrl.GET_INSTALLMENT)
    Observable<List<Installment>> getInstallments(@Query(ConstantsUrl.AMOUNT) String amount,
                                                  @Query(ConstantsUrl.PAYMENT_METHOD_ID) String paymentMethodId,
                                                  @Query(ConstantsUrl.ISSUER_ID) String issuerId);
}
