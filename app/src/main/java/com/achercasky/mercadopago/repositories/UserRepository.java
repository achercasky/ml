package com.achercasky.mercadopago.repositories;

/**
 * Created by achercasky on 24/08/2017.
 */

public class UserRepository {

    private static UserRepository instance;

    private String amount;
    private String creditCardId;
    private String creditCardName;
    private String bankId;
    private String bankName;
    private String recommendedMessage;

    public static UserRepository getInstance() {
        if(instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(String creditCardId) {
        this.creditCardId = creditCardId;
    }

    public void setCreditCardName(String creditCardName) {
        this.creditCardName = creditCardName;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }

    public String showInfo() {
        if(!bankName.isEmpty()) {
            return creditCardName + "\n" + bankName + "\n" + recommendedMessage;
        } else {
            return creditCardName + "\n" + recommendedMessage;
        }
    }

    public void reset() {
        this.amount = "";
        this.creditCardId = "";
        this.bankId = "";
        this.bankName = "";
    }
}
